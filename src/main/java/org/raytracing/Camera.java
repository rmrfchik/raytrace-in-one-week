package org.raytracing;

/**
 *
 * @author paul
 */
public class Camera {

    public Camera(Point3 lookFrom, Point3 lookAt, Vec3 vup, double vfov, double aspect_ratio, double aperture, double focus_dist) {
        this.origin = lookFrom;
        double _time0 = 0;
        double _time1 = 0;

        this.time0 = _time0;
        this.time1 = _time1;
        double theta = RT.degreesToRadians(vfov);
        double h = Math.tan(theta / 2);
        double viewport_height = 2.0 * h;
        double viewport_width = aspect_ratio * viewport_height;
        w = Vec3.unitVector(Vec3.minus(lookFrom, lookAt));
        u = Vec3.unitVector(Vec3.cross(vup, w));
        v = Vec3.cross(w, u);
        horizontal = Vec3.mult(focus_dist * viewport_width, u);
        vertical = Vec3.mult(focus_dist * viewport_height, v);
        lens_radius = aperture / 2;
        lower_left_corner = Vec3.minus(origin, Vec3.div(horizontal, 2), Vec3.div(vertical, 2), Vec3.mult(focus_dist, w));
    }

    public Camera() {
        this(new Point3(0, 0, -1), new Point3(0, 0, 0), new Vec3(0, 1, 0), 40, 1, 0, 10);
    }

    Vec3 w;
    Vec3 u;
    Vec3 v;

    Point3 origin;
    Vec3 horizontal;
    Vec3 lower_left_corner;
    Vec3 vertical;
    double lens_radius;
    double time0;
    double time1;

    public Ray get_ray(double s, double t) {
        Vec3 rd = Vec3.mult(lens_radius, Vec3.randomInUnitDisk());
        Vec3 offset = Vec3.add(Vec3.mult(rd.x(), u), Vec3.mult(rd.y(), v));
        Vec3 sh = Vec3.mult(s, horizontal);
        Vec3 tv = Vec3.mult(t, vertical);
        Vec3 stoo = Vec3.minus(Vec3.add(sh, tv), origin, offset);
        return new Ray(
                Vec3.add(origin, offset),
                Vec3.add(lower_left_corner, stoo),
                RT.random(time0, time1));
    }
}
