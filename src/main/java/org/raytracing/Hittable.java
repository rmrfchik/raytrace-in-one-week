package org.raytracing;

/**
 *
 * @author paul
 */
public interface Hittable {

    class HitRecord {

        Vec3 p;
        Vec3 normal;
        Material mat;
        double t;
        boolean frontFace;

        void setFaceNormal(final Ray r, final Vec3 outwardNormal) {
            frontFace = Vec3.dot(r.dir, outwardNormal) < 0;
            normal = frontFace ? outwardNormal : outwardNormal.minus();
        }

        public void from(HitRecord o) {
            p = o.p;
            normal = o.normal;
            mat = o.mat;
            t = o.t;
            frontFace = o.frontFace;
        }
    }

    boolean hit(final Ray r, double t_min, double t_max, HitRecord rec);
}
