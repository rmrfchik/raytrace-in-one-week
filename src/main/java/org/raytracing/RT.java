package org.raytracing;

/**
 *
 * @author paul
 */
public class RT {

    public static double degreesToRadians(final double degrees) {
        return degrees * Math.PI / 180.0;
    }

    public static double clamp(double x, double min, double max) {
        if (x < min) {
            return min;
        }
        if (x > max) {
            return max;
        }
        return x;
    }

    public static double random() {
        return Math.random();
    }

    public static double random(double min, double max) {
        // Returns a random real in [min,max).
        return min + (max - min) * random();
    }

    public static int randomi(int min, int max) {
        return (int) random(min, max + 1);
    }
}
