package org.raytracing;

/**
 *
 * @author paul
 */
public class Metal implements Material {

    final Vec3 albedo;
    final double fuzz;

    public Metal(final Vec3 a, final double f) {
        this.albedo = a;
        this.fuzz = f < 1 ? f : 1;
    }

    @Override
    public boolean scatter(final Ray r_in, final Hittable.HitRecord rec, final Vec3 attenuation, final Ray scattered) {
        Vec3 reflected = Vec3.reflect(Vec3.unitVector(r_in.dir), rec.normal);
        scattered.from(new Ray(rec.p, Vec3.add(reflected, Vec3.mult(fuzz, Vec3.randomInUnitSphere()))));
        attenuation.e = albedo.e;
        return (Vec3.dot(scattered.dir, rec.normal) > 0);
    }

}
