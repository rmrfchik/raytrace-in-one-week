package org.raytracing;

/**
 *
 * @author paul
 */
public interface Material {
    public boolean scatter(final Ray r_in, Hittable.HitRecord rec, Vec3 attenuation, Ray scattered);
}
