package org.raytracing;

/**
 *
 * @author paul
 */
public class Vec3 {

    public double[] e = new double[3];
    double length = -1;

    public Vec3(double e0, double e1, double e2) {
        e[0] = e0;
        e[1] = e1;
        e[2] = e2;
    }

    public Vec3() {
        this(0, 0, 0);
    }

    public double x() {
        return e[0];
    }

    public double y() {
        return e[1];
    }

    public double z() {
        return e[2];
    }

    public Vec3 minus() {
        return new Vec3(-e[0], -e[1], -e[2]);
    }

    public Vec3 add(final Vec3 v) {
        e[0] += v.e[0];
        e[1] += v.e[1];
        e[2] += v.e[2];
        return this;
    }

    public Vec3 mult(final double t) {
        e[0] *= t;
        e[1] *= t;
        e[2] *= t;
        return this;
    }

    public Vec3 div(final double t) {
        return mult(1 / t);
    }

    public double length() {
        return Math.sqrt(lengthSquared());
    }

    public double lengthSquared() {
        if (length > 0) {
            return length;
        }
        length = e[0] * e[0] + e[1] * e[1] + e[2] * e[2];
        return length;
    }

    public boolean nearZero() {
        final double s = 1e-8;
        return (Math.abs(e[0]) < s) && (Math.abs(e[1]) < s) && (Math.abs(e[2]) < s);
    }

    public static Vec3 random() {
        return new Vec3(RT.random(), RT.random(), RT.random());
    }

    public static Vec3 random(final double min, final double max) {
        return new Vec3(RT.random(min, max), RT.random(min, max), RT.random(min, max));
    }

    public static Vec3 add(final Vec3 u, final Vec3 v) {
        return new Vec3(u.e[0] + v.e[0], u.e[1] + v.e[1], u.e[2] + v.e[2]);
    }

    public static Vec3 minus(final Vec3 u, final Vec3 v) {
        return new Vec3(
                u.e[0] - v.e[0],
                u.e[1] - v.e[1],
                u.e[2] - v.e[2]);
    }

    public static Vec3 minus(final Vec3... u) {
        double x = u[0].x();
        double y = u[0].y();
        double z = u[0].z();
        for (int i = 1; i < u.length; i++) {
            x = x - u[i].x();
            y = y - u[i].y();
            z = z - u[i].z();
        }
        return new Vec3(x, y, z);
    }

    public static Vec3 mult(final Vec3 u, final Vec3 v) {
        return new Vec3(u.e[0] * v.e[0], u.e[1] * v.e[1], u.e[2] * v.e[2]);
    }

    public static Vec3 mult(final double t, final Vec3 v) {
        return new Vec3(t * v.e[0], t * v.e[1], t * v.e[2]);
    }

    public static Vec3 div(final Vec3 v, final double t) {
        return mult(1 / t, v);
    }

    public static double dot(final Vec3 u, final Vec3 v) {
        return u.e[0] * v.e[0] + u.e[1] * v.e[1] + u.e[2] * v.e[2];
    }

    public static Vec3 cross(final Vec3 u, final Vec3 v) {
        return new Vec3(
                u.e[1] * v.e[2] - u.e[2] * v.e[1],
                u.e[2] * v.e[0] - u.e[0] * v.e[2],
                u.e[0] * v.e[1] - u.e[1] * v.e[0]);
    }

    public static Vec3 unitVector(final Vec3 v) {
        return div(v, v.length());
    }

    public static Vec3 randomInUnitDisk() {
        while (true) {
            var p = new Vec3(RT.random(-1, 1), RT.random(-1, 1), 0);
            if (p.lengthSquared() >= 1) {
                continue;
            }
            return p;
        }
    }

    public static Vec3 randomInUnitSphere() {
        while (true) {
            var p = Vec3.random(-1, 1);
            if (p.lengthSquared() >= 1) {
                continue;
            }
            return p;
        }
    }

    public static Vec3 randomUnitVector() {
        return unitVector(randomInUnitSphere());
    }

    public static Vec3 randomInHemiSphere(final Vec3 normal) {
        final Vec3 inUnitSphere = randomInUnitSphere();
        if (dot(inUnitSphere, normal) > 0.0) {
            return inUnitSphere;
        } else {
            return inUnitSphere.minus();
        }
    }

    public static Vec3 reflect(final Vec3 v, final Vec3 n) {
        return minus(v, mult(2 * dot(v, n), n));
    }

    public static Vec3 refract(final Vec3 uv, final Vec3 n, final double etai) {
        var cosTheta = Math.min(dot(uv.minus(), n), 1.0);
        final Vec3 rOutPerp = uv.add(n.mult(cosTheta)).mult(etai);
        final Vec3 rOutParallel = n.mult(-Math.sqrt(Math.abs((1.0 - rOutPerp.lengthSquared()))));
        return add(rOutPerp, rOutParallel);
    }
}
