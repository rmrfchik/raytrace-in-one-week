package org.raytracing;

/**
 *
 * @author paul
 */
public class Sphere implements Hittable{

    private final Material mat;
    private final double radius;
    private final double r2;
    private final Vec3 center;
    public Sphere(final Vec3 cen, final double r, final Material m)
    {
        this.center=cen;
        this.radius=r;
        this.mat=m;
        this.r2=radius*radius;
    }

    @Override
    public boolean hit(Ray r, double t_min, double t_max, HitRecord rec) {
        Vec3 oc=Vec3.minus(r.orig,center);
        double a=r.dir.lengthSquared();
        double half_b=Vec3.dot(oc, r.dir);
        double c=oc.lengthSquared()-r2;
        double discriminant=half_b*half_b-a*c;
        if(discriminant<0)
            return false;
        double sqrtd=Math.sqrt(discriminant);
        
        double root=(-half_b-sqrtd)/a;
        if(root<t_min|| t_max<root)
        {
            root=(-half_b+sqrtd)/a;
            if(root<t_min||t_max<root)
                return false;
        }
        rec.t=root;
        rec.p=r.at(rec.t);
        Vec3 outwardNormal=Vec3.div(Vec3.minus(rec.p, center), radius);
        rec.setFaceNormal(r, outwardNormal);
        rec.mat=mat;
        return true;
    }
    
}
