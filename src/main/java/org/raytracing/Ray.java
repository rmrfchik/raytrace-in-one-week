package org.raytracing;

/**
 *
 * @author paul
 */
public class Ray {

    Vec3 orig;
    Vec3 dir;
    double tm;

    public Ray(Vec3 orig, Vec3 dir) {
        this.orig = orig;
        this.dir = dir;
        this.tm = 0;
    }

    public Ray(Vec3 orig, Vec3 dir, double tm) {
        this.orig = orig;
        this.dir = dir;
        this.tm = tm;
    }

    public Vec3 at(double t) {
        return Vec3.add(orig, Vec3.mult(t, dir));
    }

    @Deprecated
    public Ray from(Ray o) {
        this.orig = o.orig;
        this.dir = o.dir;
        this.tm = o.tm;
        return this;
    }
}
