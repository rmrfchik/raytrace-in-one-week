package org.raytracing;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author paul
 */
public class HittableList implements Hittable {

    List<Hittable> objects = new ArrayList<>();

    public void clear() {
        objects.clear();
    }

    public void add(Hittable object) {
        objects.add(object);
    }
    public HittableList()
    {
        
    }
    public HittableList(Hittable object) {
        add(object);
    }

    @Override
    public boolean hit(final Ray r, final double t_min, final double t_max, final HitRecord rec) {
        HitRecord temp_rec = new HitRecord();
        Box<Boolean> hitAnything = new Box<>(false);
        Box<Double> closestSoFar = new Box<>(t_max);
        objects.forEach(object -> {
            if (object.hit(r, t_min, closestSoFar.get(), temp_rec)) {
                hitAnything.set(true);
                closestSoFar.set(temp_rec.t);
                rec.from(temp_rec);
            }
        });
        return hitAnything.get();
    }

}
