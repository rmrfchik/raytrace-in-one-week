package org.raytracing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author paul
 */
public class Main {

    static HittableList randomScene() {
        Vec3 c1 = new Vec3(4, 0.2, 0);
        HittableList world = new HittableList();
        var groundMaterial = new Lambertian(new Vec3(0.5, 0.5, 0.5));
        world.add(new Sphere(new Vec3(0, -1000, 0), 1000, groundMaterial));
        for (int a = -11; a < 11; a++) {
            for (int b = -11; b < 11; b++) {
                double choose_mat = RT.random();
                Vec3 center = new Vec3(a + 0.9 * RT.random(), 0.2, b + 0.9 * RT.random());
                if (Vec3.minus(center, c1).length() > 0.9) {
                    Material sphereMat;
                    if (choose_mat < 0.8) {
                        Vec3 albedo = Vec3.mult(Vec3.random(), Vec3.random());
                        sphereMat = new Lambertian(albedo);
                        world.add(new Sphere(center, 0.2, sphereMat));
                    } else if (choose_mat < 0.95) {
                        Vec3 albedo = Vec3.random(0.5, 1);
                        double fuzz = RT.random(0, 0.5);
                        sphereMat = new Metal(albedo, fuzz);
                        world.add(new Sphere(center, 0.2, sphereMat));
                    } else {
                        sphereMat = new Dielectric(1.5);
                        world.add(new Sphere(center, 0.2, sphereMat));
                    }
                }
            }
        }
        Material material1 = new Dielectric(1.5);
        world.add(new Sphere(new Vec3(0, 1, 0), 1.0, material1));

        Material material2 = new Lambertian(new Vec3(0.4, 0.2, 0.1));
        world.add(new Sphere(new Vec3(-4, 1, 0), 1.0, material2));

        Material material3 = new Metal(new Vec3(0.7, 0.6, 0.5), 0);
        world.add(new Sphere(new Vec3(4, 1, 0), 1.0, material3));
        return world;
    }
    static Vec3 color0 = new Vec3(0, 0, 0);
    static Vec3 color1 = new Vec3(1, 1, 1);
    static Vec3 color2 = new Vec3(0.5, 0.7, 1);

    static Vec3 ray_color(Ray r, Hittable world, int depth) {
        Hittable.HitRecord rec = new Hittable.HitRecord();
        if (depth <= 0) {
            return color0;
        }
        if (world.hit(r, 0.001, Double.MAX_VALUE, rec)) {
            Ray scattered = new Ray(null, null);
            Vec3 attenuation = new Vec3();
            if (rec.mat.scatter(r, rec, attenuation, scattered)) {
                return Vec3.mult(attenuation, ray_color(scattered, world, depth - 1));
            }
            return color0;
        }
        Vec3 unit_direction = Vec3.unitVector(r.dir);
        var t = 0.5 * (unit_direction.y() + 1.0);
        return Vec3.add(Vec3.mult(1.0 - t, color1), Vec3.mult(t, color2));
    }

    public static void write_color(PrintStream file, Vec3 color, int sample_per_pixel) {
        var r = color.x();
        var g = color.y();
        var b = color.z();

        var scale = 1.0 / sample_per_pixel;
        r = Math.sqrt(scale * r);
        g = Math.sqrt(scale * g);
        b = Math.sqrt(scale * b);
        file.println(""
                + ((int) (256 * RT.clamp(r, 0.0, 0.999))) + " "
                + ((int) (256 * RT.clamp(g, 0.0, 0.999))) + " "
                + ((int) (256 * RT.clamp(b, 0.0, 0.999))));
    }

    public static void main(String... arg) {
        try {
            System.in.read();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        long t0=System.currentTimeMillis();
        final double aspect_ratio = 16.0 / 9.0;
        final int image_width = 1200;
        final int image_height = (int) (image_width / aspect_ratio);
        final int samples_per_pixel = 10;
        final int max_depth = 50;
        var world = randomScene();
        Point3 lookFrom = new Point3(13, 2, 3);
        Point3 lookAt = new Point3(0, 0, 0);

        Vec3 vup = new Vec3(0, 1, 0);
        double dist_to_focus = 10.0;
        double aperture = 0.1;

        Camera cam = new Camera(lookFrom, lookAt, vup, 20, aspect_ratio, aperture, dist_to_focus);

        PrintStream file;
        try {
            file = new PrintStream("c:/temp/p.ppm");
            file.println("P3\n" + image_width + " " + image_height + "\n255");
            for (int j = image_height - 1; j >= 0; --j) {
                System.err.println("Scanlines remaining " + j);
                System.err.flush();
                for (int i = 0; i < image_width; ++i) {
                    Vec3 pixel_color = new Vec3(0, 0, 0);
                    for (int s = 0; s < samples_per_pixel; ++s) {
                        var u = (i + RT.random()) / (image_width - 1);
                        var v = (j + RT.random()) / (image_height - 1);
                        Ray r = cam.get_ray(u, v);
                        pixel_color.add(ray_color(r, world, max_depth));
                    }
                    write_color(file, pixel_color, samples_per_pixel);
                }
            }
            System.out.println("Time "+(System.currentTimeMillis()-t0)/1000+" s");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
