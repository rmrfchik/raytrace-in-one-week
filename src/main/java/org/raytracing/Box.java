package org.raytracing;

/**
 *
 * @author paul
 */
public class Box<T> {

    T value;

    public Box(T v) {
        this.value = v;
    }

    public T get() {
        return value;
    }

    public void set(T v) {
        this.value = v;
    }

}
