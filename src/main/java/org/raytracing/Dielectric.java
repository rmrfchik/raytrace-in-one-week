package org.raytracing;

/**
 *
 * @author paul
 */
public class Dielectric implements Material {

    double ir;

    public Dielectric(final double ir) {
        this.ir = ir;
    }

    @Override
    public boolean scatter(Ray r_in, Hittable.HitRecord rec, Vec3 attenuation, Ray scattered) {
        attenuation.e = new double[]{1.0, 1.0, 1.0};
        double refractionRatio = rec.frontFace ? (1.0 / ir) : ir;
        Vec3 unitDir = Vec3.unitVector(r_in.dir);
        double cos_theta = Math.min(Vec3.dot(unitDir.minus(), rec.normal), 1.0);
        double sin_theta = Math.sqrt(1.0 - cos_theta * cos_theta);

        boolean cannot_refract = refractionRatio * sin_theta > 1.0;
        Vec3 direction;
        if (cannot_refract || reflectance(cos_theta, refractionRatio) > RT.random()) {
            direction = Vec3.reflect(unitDir, rec.normal);
        } else {
            direction = Vec3.refract(unitDir, rec.normal, refractionRatio);
        }
        scattered.from(new Ray(rec.p, direction));
        return true;
    }

    static double reflectance(final double cosine, final double ref_idx) {
        double r0 = (1 - ref_idx) / (1 + ref_idx);
        r0 = r0 * r0;
        return r0 + (1 - r0) * Math.pow((1 - cosine), 5);
    }
}
