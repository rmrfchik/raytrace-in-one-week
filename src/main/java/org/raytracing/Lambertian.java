package org.raytracing;

/**
 *
 * @author paul
 */
public class Lambertian implements Material {

    Vec3 albedo;

    public Lambertian(final Vec3 albedo) {
        this.albedo = albedo;
    }

    @Override
    public boolean scatter(final Ray r_in, final Hittable.HitRecord rec, final Vec3 attenuation, final Ray scattered) {
        Vec3 scatterDirection = Vec3.add(rec.normal, Vec3.randomUnitVector());
        if (scatterDirection.nearZero()) {
            scatterDirection = rec.normal;
        }
        scattered.from(new Ray(rec.p, scatterDirection));
        attenuation.e = albedo.e;
        return true;
    }
}
